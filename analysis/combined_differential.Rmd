---
title: "Combining methylation and gene expression results"
author: "Reka Toth"
date: '`r format(Sys.time(), "%d %B, %Y")`'
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---


```{r libraries, message=FALSE, warning=FALSE}

library(methrix)
library(annotatr)
library(DESeq2)
library(dplyr)
library(tibble)
library(ggplot2)
library(ggrepel)
library(ChIPseeker)
library(EnsDb.Mmusculus.v79)
library(circlize)
library(ComplexHeatmap)
```

##  Folders

```{r folders, message=FALSE, warning=FALSE}
###########libraries and functions#############
if (grepl("Windows", Sys.getenv("OS"))){
  PATH ="V:/"} else {
    PATH ="/C010-projects/"}
if (grepl("Windows", Sys.getenv("OS"))){
  PATH_Y="N:/"} else {
    PATH_Y="/C010-datasets/"}

DATA_RNA = paste0(PATH, "Reka/33_CoO_lung/RNASeq_analysis/data/")
DATA_WGBS = paste0(PATH, "Reka/33_CoO_lung/CoO_Lung_Cancer/data/")

DATA  = paste0(PATH, "Reka/33_CoO_lung/CoO_Lung_Cancer/data/")
RESULT = "output/"
CODE = "code/"
CALLS = paste0(PATH_Y, "External/Sotillo_mouse_lung_cell/RNASeq_10_12/")

```

## Open the results of differential methylation calling and differential expression

```{r, message=FALSE, warning=FALSE}

DMRs <- readRDS(file.path(DATA_WGBS, "DMRs_new_groups.RDS"))
DMLs <- readRDS(file.path(DATA_WGBS, "DMLs_new_groups.RDS"))
comparisons_all <- names(DMRs)


labels <- readRDS(file.path(DATA_WGBS, "labels.RDS"))
labels <- rbind(labels, data.frame(cell="", first="MsCCCP_tumor", sec="MsSPC_tumor", comparisons="CCSP_tumor"))
rownames(labels) <- labels$comparisons

labels <- labels[comparisons_all,]



dds <- readRDS( file = file.path(DATA_RNA, "dss.RDS"))
res <- readRDS( file = file.path(DATA_RNA, "res.RDS"))


```


### Select comparisons that are in both results

```{r, message=FALSE, warning=FALSE}
names(DMRs) <- gsub("MsCCSP_MsSPC_control_comparison", "MsSPC_normal_MsCCSP_normal", names(DMRs))
names(DMRs) <- gsub("MsCCSP_MsSPC_tumor_comparison", "MsSPC_tumor_MsCCSP_tumor", names(DMRs))
names(DMRs) <- gsub("MsCCSP_tumor_control_comparison", "MsCCSP_tumor_MsCCSP_normal", names(DMRs))
names(DMRs) <- gsub("MsSPC_tumor_control_comparison", "MsSPC_tumor_MsSPC_normal", names(DMRs))

comparisons <- names(dds)

```

# Association between methylation and gene expression{.tabset .tabset-fade .tabset-pills}

```{r echo=FALSE, message=FALSE, warning=FALSE, results='asis'}

require(TxDb.Mmusculus.UCSC.mm10.knownGene)
txdb <- TxDb.Mmusculus.UCSC.mm10.knownGene

library(org.Mm.eg.db)
x <- org.Mm.egENSEMBL
mapped_genes <- mappedkeys(x)
x <- as.data.frame(x[mapped_genes])


methylation <- list()
methylation_res <- list()
combined_dataset <- list()
for (comp in comparisons){
  
    cat('\n')
  
  cat("## Comparisons",
      paste0(gsub("(^Ms[[:alpha:]]+)\\_(normal|tumor)_(Ms[[:alpha:]]+)_(normal|tumor)", "\\1", comp), " ",
             gsub("(^Ms[[:alpha:]]+)\\_(normal|tumor)_(Ms[[:alpha:]]+)_(normal|tumor)", "\\2", comp), " vs. ", 
             gsub("(^Ms[[:alpha:]]+)\\_(normal|tumor)_(Ms[[:alpha:]]+)_(normal|tumor)", "\\3", comp), " ",
             gsub("(^Ms[[:alpha:]]+)\\_(normal|tumor)_(Ms[[:alpha:]]+)_(normal|tumor)", "\\4", comp),
      "\n"))
  
    methylation[[comp]]  <- makeGRangesFromDataFrame(DMRs[[comp]] , keep.extra.columns = T)
    genome(methylation[[comp]] ) <- rep("mm10", length(genome(methylation[[comp]] )))

  #dplyr::left_join(rownames_to_column(as.data.frame(res[[comp]])), as.data.frame(x), by=c("rowname"="ensembl_id"))
  peakAnno <-
      annotatePeak(methylation[[comp]],
                   tssRegion = c(-3000, 3000),
                   TxDb = txdb, verbose = FALSE)
  methylation_res[[comp]] <- peakAnno@anno %>%
    as.data.frame %>%
    dplyr::filter(grepl(annotation, pattern = "Promoter"))
  
  combined_dataset[[comp]] <- res[[comp]] %>%
    as.data.frame %>%
    rownames_to_column %>%
      dplyr::filter(!is.na(pvalue)) %>%
    dplyr::left_join(y=as.data.frame(x), by=c("rowname"="ensembl_id")) %>% 
      inner_join(y=methylation_res[[comp]], by=c("gene_id"="geneId")) %>%
    arrange(desc(abs(log2FoldChange)), desc(abs(diff.Methy))) %>%
    dplyr::mutate(labels=c(symbol[1:30], rep(NA, length(symbol)-30)))
if (comp %in% c("MsSPC_normal_MsCCSP_normal", "MsSPC_tumor_MsCCSP_tumor")){
  combined_dataset[[comp]]$diff.Methy <- -(combined_dataset[[comp]]$diff.Methy)
}
  write.table(combined_dataset[[comp]], file.path(DATA, paste0(comp, "_different_genes.txt")), row.names= FALSE, quote = F, sep="\t") 

  g <- ggplot(data= combined_dataset[[comp]], aes(log2FoldChange, diff.Methy)) +
  stat_density_2d(aes(fill = ..density..), geom = "raster", contour = FALSE) +
  scale_fill_distiller(palette=1, direction=1) +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  xlab("log2 fold change \n (if positive, higher in the first category)")+
  ylab("Diff. methylation \n (if positive, higher in the first category)")+
  theme(
    legend.position='none'
  )+geom_point(data=combined_dataset[[comp]][1:30,], aes(color=-log10(padj)),size=2)+theme_bw()+geom_text_repel( aes(label=labels))+
  scale_color_gradient(low = "white",
  high = "darkred",
   name="-log10 adj. p",
  na.value = "grey50")


print(g)
        cat("\n")

        
}

```

# Candidate pathway: lung epithelial cell differentiation

(GO:0060487)[http://amigo.geneontology.org/amigo/term/GO:0060487]

## Methylation and gene expression values

### Only genes with promoter DMRs

```{r echo=FALSE, message=FALSE, warning=FALSE, result='asis'}
library(annotate)
x <- org.Mm.egGO2ALLEGS
mapped_genes <- mappedkeys(x)
x <- as.data.frame(x[mapped_genes])
genelist <- x[x$go_id=="GO:0060487","gene_id"]
genelist <- c(genelist, "20674", "72434", "23795")
genelist <- unique(genelist)

x <- org.Mm.egENSEMBL
mapped_genes <- mappedkeys(x)
x <- as.data.frame(x[mapped_genes])
genelist <- x[x$gene_id %in% genelist,]
genelist$symbol <- getSYMBOL(genelist$gene_id, data='org.Mm.eg')


all_dmrs <- do.call(rbind.data.frame, lapply(methylation_res, function(x) x[x$geneId %in% genelist$gene_id,]))
all_dmrs <- makeGRangesFromDataFrame(all_dmrs, keep.extra.columns = F)
all_dmrs <- GenomicRanges::reduce(all_dmrs)

peakAnno <-
      annotatePeak(all_dmrs,
                   tssRegion = c(-3000, 500),
                   TxDb = txdb, verbose = FALSE)
peakAnno <- peakAnno@anno
peakAnno <- peakAnno[grep("Promoter", peakAnno$annotation),]

meth <- readRDS(paste0(DATA, "no_snpsXY_methrix.RDS"))
methylation <- as.data.frame(methrix::get_region_summary(meth, peakAnno))


new_groups <-  readRDS(paste0(DATA, "new_tumor_groups.RDS"))
new_groups[["HOPX"]] <- NULL

annotation <- data.frame(group_old = paste0(meth@colData$cell_type, "_", meth@colData$sample_name), stringsAsFactors = F)
annotation$group_new <- NA
annotation$group_new[grep("(MsSPC|MsCCSP)_control",annotation$group_old)] <- annotation$group_old[grep("(MsSPC|MsCCSP)_control",annotation$group_old)]
rownames(annotation) <- meth@colData$full_name
annotation$group_new[rownames(annotation) %in% new_groups$CCSP] <- "MsCCSP_tumor"
annotation$group_new[rownames(annotation) %in% new_groups$SPC] <- "MsSPC_tumor"
annotation <- annotation[!is.na(annotation$group_new),]

methylation <- methylation[,c(1:4, which(colnames(methylation) %in% rownames(annotation)))]

methylation$MsCCSP_control <- apply(methylation[,rownames(annotation)[annotation$group_new=="MsCCSP_control"]], 1, mean, na.rm=T)
methylation$MsSPC_control <- apply(methylation[,rownames(annotation)[annotation$group_new=="MsSPC_control"]], 1, mean, na.rm=T)
methylation$MsCCSP_tumor <- apply(methylation[,rownames(annotation)[annotation$group_new=="MsCCSP_tumor"]], 1, mean, na.rm=T)
methylation$MsSPC_tumor <- apply(methylation[,rownames(annotation)[annotation$group_new=="MsSPC_tumor"]], 1, mean, na.rm=T)
methylation <- methylation[-grep("[[:digit:]]+", colnames(methylation))]
mcols(peakAnno) <- cbind(peakAnno, methylation)

exprs <- as.data.frame(readRDS(file.path(DATA_RNA, "ge_matrix.RDS")))
exprs <- exprs %>%
  rownames_to_column %>%
  as_tibble %>%
  dplyr::select(!contains("rfp"))

#symbols <-   ensembldb::select(EnsDb.Mmusculus.v79, keys= genelist, keytype = "ENTREZID", columns = c("SYMBOL", "ENTREZID",  "GENEID"))

exprs <- exprs %>%
      dplyr::inner_join(y=genelist, by=c("rowname"="ensembl_id")) %>% 
      mutate(rowname=NULL) %>%
  dplyr::filter(!duplicated(symbol)) %>% 
      as.data.frame

exprs <- exprs[exprs$gene_id %in% genelist$gene_id,]
exprs$MsSPC_control <- apply(exprs[,grep("MSSPC-N", colnames(exprs))], 1, mean, na.rm=T)
exprs$MsSPC_tumor <- apply(exprs[,grep("MSSPC-T", colnames(exprs))], 1, mean, na.rm=T)
exprs$MsCCSP_control <- apply(exprs[,grep("MSCCSP-N", colnames(exprs))], 1, mean, na.rm=T)
exprs$MsCCSP_tumor <- apply(exprs[,grep("MSCCSP-T", colnames(exprs))], 1, mean, na.rm=T)

exprs <- exprs[,c("symbol", "MsSPC_control", "MsSPC_tumor", "MsCCSP_control", "MsCCSP_tumor")]
#exprs$type <- "expression"
mat = as.matrix(exprs[, -1])
base_mean = rowMeans(mat)
mat_scaled = t(apply(mat, 1, scale))
exprs_scaled <- exprs
exprs_scaled[,-1] <- mat_scaled 

peakAnno <- merge(genelist[,c("symbol", "gene_id")], peakAnno, by.x="gene_id", by.y="geneId", all.y=T, sort=F) 
peakAnno <- peakAnno[,c("symbol", "MsSPC_control", "MsSPC_tumor", "MsCCSP_control", "MsCCSP_tumor")]
#exprs <- gather(exprs, key="sample", value="expression", MsSPC_control, MsSPC_tumor, MsCCSP_control, MsCCSP_tumor)
#peakAnno <- gather(as.data.frame(peakAnno)[,c("geneId", "MsSPC_control", "MsSPC_tumor", "MsCCSP_control", "MsCCSP_tumor")], key="sample", value="methylation", MsSPC_control, MsSPC_tumor, MsCCSP_control, MsCCSP_tumor)

#combined_dataset <- merge(exprs, peakAnno, by.x=c("ENTREZID", "sample"), by.y=c("geneId", "sample"), sort=F, all=T)

combined_dataset_scaled <- merge(exprs_scaled, peakAnno, by.x=c("symbol"), by.y=c("symbol"), sort=F, all=T)
combined_dataset_scaled <- combined_dataset_scaled[complete.cases(combined_dataset_scaled),]
combined_dataset <- merge(exprs, peakAnno, by.x=c("symbol"), by.y=c("symbol"), sort=F, all=T)

col_exprs = colorRamp2(c(-2,0, 2), c("red", "black", "green"))


ha_1 = Heatmap(as.matrix(combined_dataset_scaled[,grep(".y", colnames(combined_dataset_scaled), fixed=T)]), cluster_rows = T, cluster_columns = F, height =  unit(2, "mm")*nrow(combined_dataset_scaled),row_labels=combined_dataset_scaled$symbol, width = unit(6,"mm")*4, row_km = 4, heatmap_legend_param=list(title="Methylation", direction = "horizontal", position="topright"), column_labels = gsub(".y", "", colnames(combined_dataset_scaled[,grep(".y", colnames(combined_dataset_scaled), fixed=T)])))

ha_2 = Heatmap(as.matrix(combined_dataset_scaled[,grep(".x", colnames(combined_dataset_scaled))]), cluster_rows = T, cluster_columns = F, height =  unit(2, "mm")*nrow(combined_dataset_scaled), width = unit(6,"mm")*4, col=col_exprs, row_km = 5, heatmap_legend_param=list(title="Expression", direction = "horizontal",  position="topright"), column_labels = gsub(".x", "", colnames(combined_dataset_scaled[,grep(".x", colnames(combined_dataset_scaled), fixed=T)])))

draw(ha_2+ha_1)

```


## Methylation and gene expression differences

Methylation difference in genes with no promoter DMRs is shown as 0.

```{r echo=FALSE, message=FALSE, warning=FALSE, result='asis', fig.height=11}


combined_dataset$MsSPC_control_tumor.x <- combined_dataset$MsSPC_control.x-combined_dataset$MsSPC_tumor.x 
combined_dataset$MsCCSP_control_tumor.x <- combined_dataset$MsCCSP_control.x-combined_dataset$MsCCSP_tumor.x 
combined_dataset$MsSPC_control_MsCCSP_control.x <- combined_dataset$MsSPC_control.x-combined_dataset$MsCCSP_control.x 
combined_dataset$MsSPC_tumor_MsCCSP_tumor.x <- combined_dataset$MsSPC_tumor.x-combined_dataset$MsCCSP_tumor.x 

combined_dataset$MsSPC_control_tumor.y <- combined_dataset$MsSPC_control.y-combined_dataset$MsSPC_tumor.y 
combined_dataset$MsCCSP_control_tumor.y <- combined_dataset$MsCCSP_control.y-combined_dataset$MsCCSP_tumor.y 
combined_dataset$MsSPC_control_MsCCSP_control.y <- combined_dataset$MsSPC_control.y-combined_dataset$MsCCSP_control.y 
combined_dataset$MsSPC_tumor_MsCCSP_tumor.y <- combined_dataset$MsSPC_tumor.y-combined_dataset$MsCCSP_tumor.y 

combined_dataset <- combined_dataset[,c("symbol", "MsSPC_control_tumor.x",
                                        "MsCCSP_control_tumor.x",
                                        "MsSPC_control_MsCCSP_control.x",
                                        "MsSPC_tumor_MsCCSP_tumor.x",
                                        "MsSPC_control_tumor.y",
                                        "MsCCSP_control_tumor.y",
                                        "MsSPC_control_MsCCSP_control.y",
                                        "MsSPC_tumor_MsCCSP_tumor.y")]

combined_dataset[is.na(combined_dataset)] <- 0
 
ha_1 = Heatmap(as.matrix(combined_dataset[complete.cases(combined_dataset),grep(".y", colnames(combined_dataset), fixed=T)]), cluster_rows = T, cluster_columns = F, height =  unit(4, "mm")*nrow(combined_dataset),row_labels=combined_dataset$symbol[complete.cases(combined_dataset)], width = unit(6,"mm")*4, row_km = 4, heatmap_legend_param=list(title="Methylation", direction = "horizontal", position="topright"), column_labels = gsub(".y", "", colnames(combined_dataset[,grep(".y", colnames(combined_dataset), fixed=T)])))
ha_2 = Heatmap(as.matrix(combined_dataset[complete.cases(combined_dataset),grep(".x", colnames(combined_dataset))]), cluster_rows = T, cluster_columns = F, height =  unit(4, "mm")*nrow(combined_dataset), width = unit(6,"mm")*4,  row_km = 5, heatmap_legend_param=list(title="Expression", direction = "horizontal",  position="topright"), column_labels = gsub(".x", "", colnames(combined_dataset[,grep(".x", colnames(combined_dataset), fixed=T)])))

draw(ha_2+ha_1)


```

